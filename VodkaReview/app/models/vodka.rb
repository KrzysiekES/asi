class Vodka < ActiveRecord::Base
  belongs_to :user
  belongs_to :category
  
  validates_presence_of :name, :description, :origin
    validates :name, :length => { :minimum => 2 }
    validates :description, :length => { :minimum => 2 }
    validates :origin, presence: true, :length => { :minimum => 2 }
end
