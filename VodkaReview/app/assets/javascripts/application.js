// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .
//= require jquery
//= require bootstrap-sprockets

$(document).ready(function(){
$("input").prop('required',true);
/*
$('#sprawdz').css("display", "none");
    $('#validateMe').click(function() {
        console.log($("#imie").val());
        if($("#imie").val() === undefined || $("#imie").val() === '' || $("#opis").val() === undefined || $("#opis").val() === '' || $("#pochodzenie").val() === undefined || $("#pochodzenie").val() === ''  ){
            alert("UZUPEŁNIJ WSZYSTKIE POLA");
        }else{
            document.getElementById("sprawdz").click();
        }
    });*/
});
