class VodkasController < ApplicationController
  
  before_action :find_vodka, only: [:show, :edit, :update, :destroy]
    


  
  def index
    if params[:category].blank?
      @vodkas = Vodka.all.order("created_at DESC")
    else
      @category_id = Category.find_by(name: params[:category]).id
      @vodkas = Vodka.where(:category_id => @category_id).order("created_at DESC")
    end
    
    if params[:search]
				@vodkas = Vodka.search(params[:search]).paginate(:page => params[:page], :per_page => 5)
		else
      @vodkas = Vodka.paginate(:page => params[:page], :per_page => 5)
    end
  end
  
  
  def new
    @vodka = current_user.vodkas.build
    @categories = Category.all.map{ |c| [c.name, c.id] }
  end
  
  def create
    @vodka = current_user.vodkas.build(vodka_params)
    @vodka.category_id = params[:category_id]
    if @vodka.save
      redirect_to root_path
    else
      render 'new'
    end
  end
  
  
  
  def show
    
  end
  
  def edit
    @categories = Category.all.map{ |c| [c.name, c.id] }
  end
  
  def update
    @vodka.category_id = params[:category_id]
    if @vodka.update(vodka_params)
      redirect_to vodka_path(@vodka)
    else
      render 'edit'
    end
  end
  
  def destroy
    @vodka.destroy
    redirect_to root_path
  end
  
  
  private
  
    def vodka_params
      params.require(:vodka).permit(:name, :description, :origin, :category_id, :vodka_img)
    end
  
  def find_vodka
     @vodka = Vodka.find(params[:id])
  end 
end
