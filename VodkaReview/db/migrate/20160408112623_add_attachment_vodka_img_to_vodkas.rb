class AddAttachmentVodkaImgToVodkas < ActiveRecord::Migration
  def self.up
    change_table :vodkas do |t|
      t.attachment :vodka_img
    end
  end

  def self.down
    remove_attachment :vodkas, :vodka_img
  end
end
