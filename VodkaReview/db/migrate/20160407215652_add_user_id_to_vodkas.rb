class AddUserIdToVodkas < ActiveRecord::Migration
  def change
    add_column :vodkas, :user_id, :integer
  end
end
