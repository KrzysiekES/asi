class AddCategoryIdToVodkas < ActiveRecord::Migration
  def change
    add_column :vodkas, :category_id, :integer
  end
end
